export interface Producto {
    idProducto: number;
    nombre: string | null;
    idCategoria: number | null;
    descriptionCategoria: string | null;
    stock: number | null;
    precio: number | null;
    esActivo: number | null;
}