export interface DetalleVenta {
    idProducto: number | null;
    dEscriptionProducto: number | null;
    cantidad: number | null;
    precioTexto: string | null;
    totalTexto: string | null;
}